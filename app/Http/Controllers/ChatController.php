<?php

namespace App\Http\Controllers;

use App\Events\Message;
use App\Room;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    public function chat()
    {
        return view('chat');
    }

    public function messages(Request $request)
    {
        Message::dispatch($request->all());
    }

    public function rooms(Room $room)
    {
        return view('room',['room' => $room]);
    }
}
