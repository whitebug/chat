@extends('layouts.app')

@section('content')
    @if(Auth::check())
        <div class="container">
            <div class="card">
                <div class="card-header">Чат</div>

                <div class="card-body">
                    <chat-component :room="{{$room}}" :user="{{ Auth::user() }}"></chat-component>
                </div>
            </div>
        </div>
    @endif
@endsection
